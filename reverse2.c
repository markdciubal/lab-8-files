#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>


int main(int argc, char *argv[])
{
    FILE *fp;
    if (argc == 1)
    {
        fprintf(stderr, "You must supply a file to display.\n");
        exit(2);
    }
    struct stat s1, s2;
    stat(argv[1], &s1);
    stat(argv[2], &s2);
    if (s1.st_ino == s2.st_ino)
    {
        fprintf(stderr, "The input and output files cannot be the same!\n");
        exit(4);
    }
    fp = fopen(argv[1], "w");
    if (!fp)
    {
        printf("Could not open %s to write.\n", argv[1]);
        exit(1);
    }
    else
    {
        char line[1000];
        char reversed_line[1000];
        while (fgets(line, 1000, fp) != NULL)
        {
            for (int i = strlen(line); i >= 0; i--)
            {
                //printf("%d", i);
                reversed_line[i] = line[i];
                //printf("%c", line[i]);
            }
            printf("Reversed: %s", reversed_line);
        }
        fclose(fp);
    }
}